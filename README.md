# Testra

A simple macro that lets you define Crystal specs inline rather than in a separate spec file.

## Installation

1. Add the dependency to your `shard.yml`:
```yaml
dependencies:
  testra:
    gitlab: ezrast/testra
```
2. Run `shards install`

## Usage

Use the `testra` macro to define tests inline, just like in an `it` block:

```crystal
require "testra"

module MyModule
  def self.portions(count)
    100 / count
  end

  testra "divides portions" do
    MyModule.portions(2).should eq 50
  end

  testra "can't divide by zero" do
    expect_raises(DivisionByZeroError) do
      MyModule.portions(0)
    end
  end

  testra "adds back up" do
    # Whoops, this will fail
    (MyModule.portions(3) * 3).should eq 100
  end
end
```

Inside `spec/my_module_spec.cr`, require your module after calling `Testra.engage!`:

```crystal
require "testra"

Testra.engage!

# Don't include your entry point here or else running `crystal spec`
# will execute your program.
require "../src/my_module.cr"
```

Under normal conditions, the `testra` macro simply does nothing - its contents are thrown away and no code is generated.
If `Testra.engage!` is called, the macro will instead reformat its contents into an `it` call using the standard library's `spec` functionality.

## Contributors

- [Ezra Stevens](https://gitlab.com/ezrast)

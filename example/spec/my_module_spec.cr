require "testra"

Testra.engage!

# Don't include your entry point here or else running `crystal spec`
# will execute your program.
require "../src/my_module.cr"

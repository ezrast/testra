require "testra"

module MyModule
  def self.portions(count)
    100 / count
  end

  testra "divides portions" do
    MyModule.portions(2).should eq 50
  end

  testra "can't divide by zero" do
    expect_raises(DivisionByZeroError) do
      MyModule.portions(0)
    end
  end

  testra "adds back up" do
    # Whoops, this will fail
    (MyModule.portions(3) * 3).should eq 100
  end
end

macro testra(str)
end

module Testra
  macro engage!(verbose = false)
    require "spec"

    module Testra
      struct TestCase
        getter descriptor : String
        getter block : Proc(Nil)

        def initialize(@descriptor, &@block)
        end
      end

      TEST_CASES = {} of String => Array(TestCase)
    end

    macro testra(str)
      arr = Testra::TEST_CASES[self.name] ||= [] of Testra::TestCase
      arr << Testra::TestCase.new(
        \{{ str }}
      ) { \{{yield }} }
    end

    at_exit do
      Testra::TEST_CASES.each do |thing_name, test_cases|
        describe thing_name do
          {% if verbose %}
            puts
            puts "Testing #{thing_name}:"
          {% end %}
          test_cases.each{ |tc| it tc.descriptor, &tc.block }
        end
      end
    end
  end
end
